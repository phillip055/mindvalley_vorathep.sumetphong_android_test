package com.pros.lelmasters.asyncache.request;

import android.graphics.Bitmap;
import android.util.Log;

import com.pros.lelmasters.asyncache.ICallback;
import com.pros.lelmasters.asyncache.Response;
import com.pros.lelmasters.asyncache.manager.CacheManager;
import com.pros.lelmasters.asyncache.manager.DispatchManager;
import com.pros.lelmasters.asyncache.parser.ImageParser;

import java.io.InputStream;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public class ImageRequest extends Request<Bitmap> {

    String TAG = "ImageRequestClass";

    @Override
    public void enqueue(String url,ICallback<Bitmap> callback) {
        super.enqueue(url,callback);
        if (attemptCache()){
            return;
        }
        DispatchManager.getInstance().enqueue(this, callback);
    }

    @Override
    public boolean attemptCache() {
        Bitmap bitmap;
        synchronized (CacheManager.getInstance().ImageCache) {
            bitmap = CacheManager.getInstance().ImageCache.get(getUrlHash());
        }
        if (bitmap!=null){
            Response<Bitmap> res = createResponse(bitmap,true);
            callback.onSuccess(res);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public Response<Bitmap> execute() {
        Response<Bitmap> res = null;
        ImageParser imageParser = new ImageParser();
        InputStream inputStream = this.run(this.url);
        try {
            if (inputStream != null) {
                Bitmap bitmap = imageParser.fromBody(inputStream);
                res = createResponse(bitmap,false);
                CacheManager.getInstance().ImageCache.put(getUrlHash(),bitmap);
            }
        }
        catch (Exception e){
            Log.d(TAG,"Crash" + e.getMessage());
        }
        return res;
    }
}
