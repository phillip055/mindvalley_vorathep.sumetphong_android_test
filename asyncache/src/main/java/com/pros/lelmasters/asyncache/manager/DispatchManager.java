package com.pros.lelmasters.asyncache.manager;

import android.os.AsyncTask;
import android.os.Handler;

import com.pros.lelmasters.asyncache.ICallback;
import com.pros.lelmasters.asyncache.Response;
import com.pros.lelmasters.asyncache.request.Request;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public class DispatchManager{

    private static DispatchManager instance;
    Handler handler = new Handler();

    String TAG = "DispatcherClass";

    public static void initInstance() {
        if (instance == null) {
            instance = new DispatchManager();
        }
    }

    public static DispatchManager getInstance() {
        initInstance();
        return instance;
    }

    private HashMap<String, ArrayList<ICallback>> requestQueue = new HashMap<>();

    public <T> void enqueue(final Request<T> request, final ICallback<T> callback) {
        synchronized (requestQueue) {
            if (request.isCancelled()) return;
            if (requestQueue.containsKey(request.getUrlHash())) {
                requestQueue.get(request.getUrlHash()).add(callback);
            } else {
                ArrayList<ICallback> callbacks = new ArrayList<>();
                callbacks.add(callback);
                requestQueue.put(request.getUrlHash(), callbacks);
            }
        }
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final Response<T> response = request.execute();
                    if (response.getBody() == null) {
                        FailOnUIThread(request.getUrlHash());
                    } else {
                        PassOnToUIThread(request.getUrlHash(),response);
                    }
                } catch (Exception e) {
                    FailOnUIThread(request.getUrlHash());
                }
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private <T> void PassOnToUIThread(final String url, final Response<T> res){
        synchronized (requestQueue) {
            for (final ICallback<T> callback : requestQueue.get(url)) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onSuccess(res);
                        requestQueue.get(url).remove(callback);
                    }
                });
            }
        }
    }

    private <T> void FailOnUIThread(final String url){
        synchronized (requestQueue) {
            for (final ICallback<T> callback : requestQueue.get(url)) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onFailure();
                        requestQueue.get(url).remove(callback);
                    }
                });
            }
        }
    }
}
