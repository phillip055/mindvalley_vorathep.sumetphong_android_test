package com.pros.lelmasters.asyncache.cache;

import android.util.LruCache;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public class Cache<T> extends LruCache<String, T> implements ICache<T> {

    /**
     * @param maxSize for caches that do not override {@link #sizeOf}, this is
     *                the maximum number of entries in the cache. For all other caches,
     *                this is the maximum sum of the sizes of the entries in this cache.
     */
    public Cache(int maxSize) {
        super(maxSize);
    }

    @Override
    public boolean isKeyExist(String key) {
        return this.get(key)!=null;
    }


}