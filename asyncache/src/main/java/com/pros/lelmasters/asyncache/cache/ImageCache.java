package com.pros.lelmasters.asyncache.cache;

import android.graphics.Bitmap;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public class ImageCache extends Cache<Bitmap> {

    public ImageCache(int maxSize){
        super(maxSize);
    }

    @Override
    protected int sizeOf(String key, Bitmap bitmap) {
        return bitmap.getByteCount();
    }

}