package com.pros.lelmasters.asyncache;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public class Response<T> {
    private T body;

    private boolean isFromCache = false;

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    public void setIsFromCache(boolean isFromCache) {
        this.isFromCache = isFromCache;
    }

    public boolean isFromCache(){
        return isFromCache;
    }
}