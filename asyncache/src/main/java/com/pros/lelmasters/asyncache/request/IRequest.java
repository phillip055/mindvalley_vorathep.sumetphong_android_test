package com.pros.lelmasters.asyncache.request;

import com.pros.lelmasters.asyncache.ICallback;
import com.pros.lelmasters.asyncache.Response;

import java.io.IOException;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public interface IRequest<T> {

    String getUrlHash();

    void cancel();

    boolean isCancelled();

    boolean attemptCache();

    void enqueue(String url,ICallback<T> callback);

    Response<T> execute() throws IOException;

}
