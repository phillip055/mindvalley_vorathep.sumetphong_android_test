package com.pros.lelmasters.asyncache.request;

import android.graphics.Bitmap;
import android.util.Log;

import com.pros.lelmasters.asyncache.ICallback;
import com.pros.lelmasters.asyncache.Response;
import com.pros.lelmasters.asyncache.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public class Request<T> implements IRequest<T> {
    String TAG = "BaseRequestClass";
    protected boolean isCancelled = false;
    protected String url;
    protected ICallback<T> callback;

    @Override
    public String getUrlHash() {
        return Utils.getmd5key(this.url);
    }

    @Override
    public void cancel() {
        isCancelled = true;
    }



    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    protected Response<T> createResponse(T res, boolean isFromCache){
        Response<T> response = new Response<>();
        response.setBody(res);
        response.setIsFromCache(isFromCache);
        return response;
    }

    @Override
    public boolean attemptCache() {
        return false;
    }

    @Override
    public void enqueue(String url, ICallback<T> callback) {
        this.url = url;
        this.callback = callback;
        if (this.isCancelled()){
            return;
        }
    }

    @Override
    public Response<T> execute() throws IOException {
        return null;
    }

    protected InputStream run(String urlStr) {
        try {
            URL url = new URL(urlStr);
            HttpURLConnection urlConnection;
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            return inputStream;
        } catch (Exception e) {
            return null;
        }
    }
}
