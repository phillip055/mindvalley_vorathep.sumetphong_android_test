package com.pros.lelmasters.asyncache.parser;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public interface IParser<T> {
    T fromBody(InputStream res) throws IOException;
}
