package com.pros.lelmasters.asyncache.cache;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public interface ICache<T> {

    T put (String key, T value);

    T get (String key);

    T remove(String key);

    boolean isKeyExist(String key);

    void evictAll();

}
