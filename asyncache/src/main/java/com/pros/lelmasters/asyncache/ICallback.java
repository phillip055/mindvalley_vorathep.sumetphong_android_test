package com.pros.lelmasters.asyncache;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public interface ICallback<T> {
    void onSuccess(Response<T> result);

    void onFailure();
}
