package com.pros.lelmasters.asyncache.manager;

import com.pros.lelmasters.asyncache.cache.ImageCache;
import com.pros.lelmasters.asyncache.cache.StringCache;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public class CacheManager {

    private static CacheManager instance;
    public ImageCache ImageCache;
    public StringCache StringCache;

    public static void initInstance() {
        if (instance == null) {
            instance = new CacheManager();
        }
    }

    public static CacheManager getInstance() {
        initInstance();
        return instance;
    }

    private CacheManager() {
        ImageCache = new ImageCache(10 * 1024 * 1024);
        StringCache = new StringCache(10 * 1024 * 1024);
    }
}
