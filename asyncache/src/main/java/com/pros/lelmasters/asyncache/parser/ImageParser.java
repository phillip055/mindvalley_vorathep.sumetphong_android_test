package com.pros.lelmasters.asyncache.parser;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public class ImageParser implements IParser<Bitmap>{

    @Override
    public Bitmap fromBody(InputStream res) throws IOException {
        return BitmapFactory.decodeStream(res);
    }
}
