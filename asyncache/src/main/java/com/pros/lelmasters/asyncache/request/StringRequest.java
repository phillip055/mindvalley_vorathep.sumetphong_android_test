package com.pros.lelmasters.asyncache.request;

import android.graphics.Bitmap;

import com.pros.lelmasters.asyncache.ICallback;
import com.pros.lelmasters.asyncache.Response;
import com.pros.lelmasters.asyncache.Utils;
import com.pros.lelmasters.asyncache.manager.CacheManager;
import com.pros.lelmasters.asyncache.manager.DispatchManager;
import com.pros.lelmasters.asyncache.parser.StringParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public class StringRequest extends Request<String> {

    @Override
    public void enqueue(String url,ICallback<String> callback) {
        super.enqueue(url,callback);
        if (attemptCache()){
            return;
        }
        DispatchManager.getInstance().enqueue(this, callback);
    }

    @Override
    public Response<String> execute() throws IOException {
        Response<String> res = new Response<>();
        StringParser stringParser = new StringParser();
        InputStream inputStream = this.run(this.url);
        if (inputStream != null) {
            String string = stringParser.fromBody(inputStream);
            res = createResponse(string,false);
            CacheManager.getInstance().StringCache.put(getUrlHash(),string);
        }
        return res;
    }

    @Override
    public boolean attemptCache() {
        String string;
        synchronized (CacheManager.getInstance().StringCache) {
            string = CacheManager.getInstance().StringCache.get(getUrlHash());
        }
        if (string!=null){
            Response<String> res = createResponse(string,true);
            callback.onSuccess(res);
            return true;
        }
        else {
            return false;
        }
    }
}
