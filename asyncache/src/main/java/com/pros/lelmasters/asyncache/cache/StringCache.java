package com.pros.lelmasters.asyncache.cache;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public class StringCache extends Cache<String> {

    /**
     * @param maxSize for caches that do not override {@link #sizeOf}, this is
     *                the maximum number of entries in the cache. For all other caches,
     *                this is the maximum sum of the sizes of the entries in this cache.
     */

    public StringCache(int maxSize) {
        super(maxSize);
    }

    @Override
    protected int sizeOf(String key, String value) {
        return value.getBytes().length;
    }
}
