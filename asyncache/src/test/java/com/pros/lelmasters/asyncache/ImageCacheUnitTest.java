package com.pros.lelmasters.asyncache;

import android.graphics.Bitmap;

import com.pros.lelmasters.asyncache.cache.ImageCache;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
public class ImageCacheUnitTest {

    private static final Bitmap A = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);

    private static final Bitmap B = Bitmap.createBitmap(2, 1, Bitmap.Config.ALPHA_8);

    private static final Bitmap C = Bitmap.createBitmap(3, 1, Bitmap.Config.ALPHA_8);

    private ImageCache cache;

    @Before
    public void setUp() {
        cache = new ImageCache(3);
    }

    @After
    public void tearDown() {
        cache.evictAll();
        cache = null;
    }

    @Test
    public void test() {
        cache.put("1", A);
        assertNotNull(cache.get("1"));
        assertEquals(cache.size(), 1);
        cache.put("2", B);
        assertNotNull(cache.get("1"));
        assertNotNull(cache.get("2"));
        assertEquals(cache.size(), 3);
        cache.put("3", C);
        assertNull(cache.get("1"));
        assertNull(cache.get("2"));
        assertEquals(cache.size(), 3);
        assertNotNull(cache.get("3"));
    }
}