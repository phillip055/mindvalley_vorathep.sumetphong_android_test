package com.pros.lelmasters.asyncache;

import android.graphics.Bitmap;
import android.util.Log;

import com.pros.lelmasters.asyncache.request.ImageRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */

@RunWith(RobolectricTestRunner.class)
public class AsyncRequestUnitTest {
    private String TAG = "AsyncRequestTest";
    private static final ImageRequest imageRequest = new ImageRequest();
    private static final ImageRequest imageRequest2 = new ImageRequest();
    private static final ImageRequest imageRequest3 = new ImageRequest();

    @Test
    public void testEnsureCorrectBitmapSize() {

        Log.d(TAG, "1request");

        imageRequest.enqueue("http://s2.favim.com/610/150421/adorable-animal-cute-dog-Favim.com-2670482.jpg", new ICallback<Bitmap>() {
            @Override
            public void onSuccess(Response<Bitmap> result) {
                Log.d(TAG, "1request DONE");
            }

            @Override
            public void onFailure() {

            }
        });

        Log.d(TAG, "2request");
        imageRequest2.enqueue("http://data.whicdn.com/images/12849966/large.jpg", new ICallback<Bitmap>() {
            @Override
            public void onSuccess(Response<Bitmap> result) {
                Log.d(TAG, "2request DONE");
            }

            @Override
            public void onFailure() {

            }
        });

        Log.d(TAG, "3request");
        imageRequest3.enqueue("http://cdn.litlepups.net/2016/04/07/cute-cat-eyes-wallpaper.jpg", new ICallback<Bitmap>() {
            @Override
            public void onSuccess(Response<Bitmap> result) {
                Log.d(TAG, "3request DONE");
            }

            @Override
            public void onFailure() {

            }
        });
    }


}
