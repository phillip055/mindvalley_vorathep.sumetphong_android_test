package com.pros.lelmasters.asyncache;

import android.graphics.Bitmap;

import com.pros.lelmasters.asyncache.cache.ImageCache;
import com.pros.lelmasters.asyncache.cache.StringCache;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by vorathepsumetphong on 1/22/17.
 */
@RunWith(RobolectricTestRunner.class)
public class StringCacheUnitTest {
    private static final String A = "A";

    private static final String B = "A";

    private static final String C = "A";

    private StringCache cache;

    @Before
    public void setUp() {
        cache = new StringCache(3);
    }

    @After
    public void tearDown() {
        cache.evictAll();
        cache = null;
    }

    @Test
    public void test() {
        cache.put("1", A);
        assertNotNull(cache.get("1"));
        assertEquals(cache.size(), 1);
        cache.put("2", B);
        assertNotNull(cache.get("1"));
        assertNotNull(cache.get("2"));
        assertEquals(cache.size(), 2);
        cache.put("3", C);
        assertNotNull(cache.get("1"));
        assertNotNull(cache.get("2"));
        assertEquals(cache.size(), 3);
        assertNotNull(cache.get("3"));
    }
}
