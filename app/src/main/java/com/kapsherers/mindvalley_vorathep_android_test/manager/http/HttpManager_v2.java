package com.kapsherers.mindvalley_vorathep_android_test.manager.http;

import com.pros.lelmasters.asyncache.ICallback;
import com.pros.lelmasters.asyncache.Response;
import com.pros.lelmasters.asyncache.request.StringRequest;

/**
 * Created by vorathepsumetphong on 1/22/17.
 */
public class HttpManager_v2 {
    private static HttpManager_v2 instance;

    public static HttpManager_v2 getInstance() {
        if (instance == null)
            instance = new HttpManager_v2();
        return instance;
    }

    public void getPins(ICallback iCallback){
        StringRequest stringRequest = new StringRequest();
        stringRequest.enqueue("http://pastebin.com/raw/wgkJgazE", iCallback);
    }
}
