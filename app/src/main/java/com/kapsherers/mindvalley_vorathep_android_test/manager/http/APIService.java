package com.kapsherers.mindvalley_vorathep_android_test.manager.http;

import com.kapsherers.mindvalley_vorathep_android_test.model.Pin;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public interface APIService {
    @GET("wgkJgazE")
    Call<List<Pin>> GetPins();
}
