package com.kapsherers.mindvalley_vorathep_android_test.manager;

import android.content.Context;

import com.kapsherers.mindvalley_vorathep_android_test.model.Pin;

import java.util.List;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class HomePinsManager {

    private static HomePinsManager instance;
    private List<Pin> pinList;

    public static HomePinsManager getInstance() {
        if (instance == null)
            instance = new HomePinsManager();
        return instance;
    }

    public List<Pin> getPinList() {
        return pinList;
    }

    public void setPinList(List<Pin> pinList) {
        this.pinList = pinList;
    }
}
