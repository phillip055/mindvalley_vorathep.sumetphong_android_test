package com.kapsherers.mindvalley_vorathep_android_test.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.kapsherers.mindvalley_vorathep_android_test.R;
import com.kapsherers.mindvalley_vorathep_android_test.manager.HomePinsManager;
import com.kapsherers.mindvalley_vorathep_android_test.model.Pin;
import com.kapsherers.mindvalley_vorathep_android_test.view.ListItem;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class HomePinListRecyclerAdapter extends RecyclerView.Adapter<HomePinListRecyclerAdapter.ViewHolder>  {



    public HomePinListRecyclerAdapter(){
        setHasStableIds(true);
    }
    private int lastPosition;
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ListItem item;
        public ViewHolder (ListItem postListItem){
            super(postListItem);
            this.item = postListItem;
        }
    }

    @Override
    public HomePinListRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItem v = new ListItem(parent.getContext());
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(HomePinListRecyclerAdapter.ViewHolder holder, int position) {
        Pin pin =(Pin) getItem(position);
        holder.item.setImageView(pin.getUrls().getThumb());
        holder.item.setName(pin.getId());
        setAnimation(holder.item,position);
    }

    private Object getItem(int position) {
        return HomePinsManager.getInstance().getPinList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return HomePinsManager.getInstance().getPinList().get(position).getId().hashCode();
    }

    @Override
    public int getItemCount() {
        if (HomePinsManager.getInstance().getPinList() == null) {
            return 0;
        }
        return HomePinsManager.getInstance().getPinList().size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.bottom_to_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
