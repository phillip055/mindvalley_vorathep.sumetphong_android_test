package com.kapsherers.mindvalley_vorathep_android_test.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.kapsherers.mindvalley_vorathep_android_test.view.ListItem;
import com.kapsherers.mindvalley_vorathep_android_test.manager.HomePinsManager;
import com.kapsherers.mindvalley_vorathep_android_test.model.Pin;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class HomePinListAdapter extends BaseAdapter {

    @Override
    public int getCount() {
        if (HomePinsManager.getInstance().getPinList()==null){
            return 0;
        }
        return HomePinsManager.getInstance().getPinList().size();
    }

    public Object getItem(int position) {
        return HomePinsManager.getInstance().getPinList().get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ListItem item;
        if (view != null) {
            item = (ListItem) view;
        } else {
            item = new ListItem(viewGroup.getContext());
        }
        Pin dao = (Pin) getItem(i);
        item.setImageView(dao.getUrls().getThumb());
        item.setName(dao.getId());
        return item;
    }

}
