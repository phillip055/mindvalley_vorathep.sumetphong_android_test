package com.kapsherers.mindvalley_vorathep_android_test.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.kapsherers.mindvalley_vorathep_android_test.R;
import com.kapsherers.mindvalley_vorathep_android_test.fragment.HomeFragment;
import com.pros.lelmasters.asyncache.manager.CacheManager;

/**
 * Created by vorathepsumetphong on 1/20/17.
 */
public class HomeActivity extends AppCompatActivity {
    Toolbar toolbar;

    String TAG = "MainActivityClass";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initInstances();
        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, HomeFragment.newInstance(), "fragment_main")
                    .commit();
        }
    }

    private void initInstances(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear_cache:
                CacheManager.getInstance().ImageCache.evictAll();
                CacheManager.getInstance().StringCache.evictAll();
                break;
        }
        return true;
    }
}
