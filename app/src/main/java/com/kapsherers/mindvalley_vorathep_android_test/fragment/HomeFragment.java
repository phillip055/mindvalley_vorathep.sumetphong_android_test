package com.kapsherers.mindvalley_vorathep_android_test.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.kapsherers.mindvalley_vorathep_android_test.R;
import com.kapsherers.mindvalley_vorathep_android_test.adapter.HomePinListAdapter;
import com.kapsherers.mindvalley_vorathep_android_test.adapter.HomePinListRecyclerAdapter;
import com.kapsherers.mindvalley_vorathep_android_test.manager.HomePinsManager;
import com.kapsherers.mindvalley_vorathep_android_test.manager.http.HttpManager;
import com.kapsherers.mindvalley_vorathep_android_test.manager.http.HttpManager_v2;
import com.kapsherers.mindvalley_vorathep_android_test.model.Pin;
import com.kapsherers.mindvalley_vorathep_android_test.view.EasterEggDialog;
import com.kapsherers.mindvalley_vorathep_android_test.view.HomeRecyclerGridView;
import com.kapsherers.mindvalley_vorathep_android_test.view.SpacesItemDecoration;
import com.pros.lelmasters.asyncache.ICallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class HomeFragment extends Fragment {

    private String TAG = "HomeFragment";
    HomeRecyclerGridView recyclerView;
    HomePinListRecyclerAdapter mAdapter;
    FloatingActionButton floatingActionButton;
    EasterEggDialog dialog;
    SwipeRefreshLayout swipeRefreshLayout;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        initInstances(rootView);
        loadData();
        return rootView;
    }

    private void loadData() {
//        Call<List<Pin>> call = HttpManager.getInstance().getService().GetPins();
//        call.enqueue(new Callback<List<Pin>>() {
//            @Override
//            public void onResponse(Response<List<Pin>> response) {
//                List<Pin> pins = response.body();
//                Collections.shuffle(pins);
//                HomePinsManager.getInstance().setPinList(pins);
//                swipeRefreshLayout.setRefreshing(false);
//                mAdapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                Toast.makeText(getContext(), "Failed to reach resource", Toast.LENGTH_SHORT).show();
//                swipeRefreshLayout.setRefreshing(false);
//            }
//        });

        HttpManager_v2.getInstance().getPins(new ICallback<String>() {
            @Override
            public void onSuccess(com.pros.lelmasters.asyncache.Response<String> result) {
                String isFromCache = result.isFromCache() ? "From Cache" : "New Data";
                Toast.makeText(getContext(), isFromCache, Toast.LENGTH_SHORT).show();
                JsonArray jsonArray = new JsonParser().parse(result.getBody()).getAsJsonArray();
                List<Pin> pinList = new ArrayList<>();
                for(int i=0; i < jsonArray.size(); i++){
                    Gson gson = new Gson();
                    Pin pin = gson.fromJson(jsonArray.get(i),Pin.class);
                    pinList.add(pin);
                }
                Collections.shuffle(pinList);
                HomePinsManager.getInstance().setPinList(pinList);
                swipeRefreshLayout.setRefreshing(false);
                mAdapter.notifyDataSetChanged();
            }
            @Override
            public void onFailure() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }



    private void initInstances(View rootView) {
        recyclerView = (HomeRecyclerGridView) rootView.findViewById(R.id.recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        dialog  = new EasterEggDialog(getContext());
        floatingActionButton = (FloatingActionButton) rootView.findViewById(R.id.myFAB);
        mAdapter = new HomePinListRecyclerAdapter();
        recyclerView.setAdapter(mAdapter);
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
//                if (dy > 0)
//                    floatingActionButton.hide();
//                else if (dy < 0)
//                    floatingActionButton.show();
//            }
//        });
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                loadData();
            }
        });
    }

}
