package com.kapsherers.mindvalley_vorathep_android_test.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.kapsherers.mindvalley_vorathep_android_test.R;

/**
 * Created by vorathepsumetphong on 1/22/17.
 */

/**
 * This class is used to make ImageDialogs to show memes
 */
public class ImageDialog extends Dialog{

    ImageView imageView;

    public ImageDialog(Context context) {
        super(context);
        initInstances();
    }

    public ImageDialog(Context context, int themeResId) {
        super(context, themeResId);
        initInstances();
    }

    protected ImageDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        initInstances();
    }

    private void initInstances(){
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setCancelable(true);
        imageView = new ImageView(getContext());
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
        addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
    }

    public void setDrawable(int drawable){
        imageView.setImageDrawable(ContextCompat.getDrawable(getContext(),drawable));
    }
}
