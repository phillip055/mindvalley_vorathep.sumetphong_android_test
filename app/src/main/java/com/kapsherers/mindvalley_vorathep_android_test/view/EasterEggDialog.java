package com.kapsherers.mindvalley_vorathep_android_test.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kapsherers.mindvalley_vorathep_android_test.R;

/**
 * Created by vorathepsumetphong on 1/22/17.
 */

/**
 * This class is used to show dialog on Home
 */
public class EasterEggDialog extends AlertDialog {


    public EasterEggDialog(Context context) {
        super(context);
        initInstances();
    }

    private void initInstances(){
        setCancelable(true);
        setTitle(getContext().getString(R.string.dialog_title));
        setMessage(getContext().getString(R.string.dialog_desc));
        setButton(BUTTON_POSITIVE, getContext().getString(R.string.hi), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ImageDialog imageDialog = new ImageDialog(getContext());
                imageDialog.setDrawable(R.drawable.me);
                imageDialog.show();
            }
        });
        setButton(BUTTON_NEGATIVE, getContext().getString(R.string.bye), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ImageDialog imageDialog = new ImageDialog(getContext());
                imageDialog.setDrawable(R.drawable.failed_city);
                imageDialog.show();
            }
        });
    }
}
