package com.kapsherers.mindvalley_vorathep_android_test.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kapsherers.mindvalley_vorathep_android_test.R;
import com.pros.lelmasters.asyncache.ICallback;
import com.pros.lelmasters.asyncache.Response;
import com.pros.lelmasters.asyncache.request.ImageRequest;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class ListItem extends CardView {
    TextView name;
    ImageView thumbnail;
    public ListItem(Context context) {
        super(context);
        initInflate();
        initIntances();
    }

    public ListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initIntances();
    }

    public ListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initIntances();
    }

    private void initIntances() {
        name = (TextView) findViewById(R.id.name);
        thumbnail = (ImageView) findViewById(R.id.img);
    }

    private void initInflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.list_item, this);
    }
    public void setName(String name) {
        this.name.setText(name);
    }

    public void setImageView(String url){
        thumbnail.setImageBitmap(null);
        ImageRequest imageRequest = new ImageRequest();
        imageRequest.enqueue(url, new ICallback<Bitmap>() {
            @Override
            public void onSuccess(Response<Bitmap> result) {
                thumbnail.setImageBitmap(result.getBody());
            }

            @Override
            public void onFailure() {
                Toast.makeText(getContext(), R.string.died, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
