package com.kapsherers.mindvalley_vorathep_android_test.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.kapsherers.mindvalley_vorathep_android_test.adapter.HomePinListRecyclerAdapter;

/**
 * Created by vorathepsumetphong on 1/22/17.
 */
public class HomeRecyclerGridView extends RecyclerView {

    private final int spanCount = 2;
    private final int spacing = 25;
    private final boolean includeEdge = true;
    RecyclerView.LayoutManager layoutManager;
    public HomeRecyclerGridView(Context context) {
        super(context);
    }

    public HomeRecyclerGridView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initInstances();
    }

    public HomeRecyclerGridView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initInstances();
    }

    private void initInstances(){
        addItemDecoration(new SpacesItemDecoration(spanCount, spacing, includeEdge));
        layoutManager = new GridLayoutManager(getContext(),2);
        setLayoutManager(layoutManager);
    }
}
