package com.kapsherers.mindvalley_vorathep_android_test.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class UserLinks {

    @SerializedName("self")
    private String self;


    @SerializedName("html")
    private String html;


    @SerializedName("download")
    private String download;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    @SerializedName("likes")
    private String likes;
}
