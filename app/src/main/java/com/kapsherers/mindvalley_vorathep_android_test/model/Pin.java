package com.kapsherers.mindvalley_vorathep_android_test.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class Pin {
    @SerializedName("id")
    private String id;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("width")
    private int width;
    @SerializedName("height")
    private int height;
    @SerializedName("color")
    private String color;
    @SerializedName("likes")
    private int likes;
    @SerializedName("liked_by_user")
    private boolean liked_by_user;
    @SerializedName("urls")
    private PinUrls urls;
    @SerializedName("categories")
    private List<PinCategory> categories;
    @SerializedName("current_user_collections")
    private List<String> current_user_collections;

    public PinLinks getLinks() {
        return links;
    }

    public void setLinks(PinLinks links) {
        this.links = links;
    }

    public List<String> getCurrent_user_collections() {
        return current_user_collections;
    }

    public void setCurrent_user_collections(List<String> current_user_collections) {
        this.current_user_collections = current_user_collections;
    }

    public List<PinCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<PinCategory> categories) {
        this.categories = categories;
    }

    public PinUrls getUrls() {
        return urls;
    }

    public void setUrls(PinUrls urls) {
        this.urls = urls;
    }

    public boolean isLiked_by_user() {
        return liked_by_user;
    }

    public void setLiked_by_user(boolean liked_by_user) {
        this.liked_by_user = liked_by_user;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @SerializedName("links")

    private PinLinks links;


}
