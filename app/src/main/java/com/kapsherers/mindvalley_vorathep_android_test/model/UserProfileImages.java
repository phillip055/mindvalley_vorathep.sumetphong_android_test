package com.kapsherers.mindvalley_vorathep_android_test.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class UserProfileImages {

    @SerializedName("small")
    private String small;

    @SerializedName("medium")
    private String medium;

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    @SerializedName("large")
    private String large;

}
