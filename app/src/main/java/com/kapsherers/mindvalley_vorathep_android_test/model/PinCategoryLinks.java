package com.kapsherers.mindvalley_vorathep_android_test.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class PinCategoryLinks {

    @SerializedName("self")
    private String self;


    @SerializedName("photos")
    private String photos;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }
}
