package com.kapsherers.mindvalley_vorathep_android_test.model;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class User {

    private String id;

    private String username;

    private String name;

    private UserProfileImages profile_image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserProfileImages getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(UserProfileImages profile_image) {
        this.profile_image = profile_image;
    }

    public UserLinks getUserLinks() {
        return userLinks;
    }

    public void setUserLinks(UserLinks userLinks) {
        this.userLinks = userLinks;
    }

    private UserLinks userLinks;
}
