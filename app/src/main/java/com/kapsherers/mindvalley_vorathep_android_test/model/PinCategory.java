package com.kapsherers.mindvalley_vorathep_android_test.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class PinCategory {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("photo_count")
    private int photo_count;

    @SerializedName("links")
    private PinCategoryLinks links;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPhoto_count() {
        return photo_count;
    }

    public void setPhoto_count(int photo_count) {
        this.photo_count = photo_count;
    }

    public PinCategoryLinks getLinks() {
        return links;
    }

    public void setLinks(PinCategoryLinks links) {
        this.links = links;
    }
}
