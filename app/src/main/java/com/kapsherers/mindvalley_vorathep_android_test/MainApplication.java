package com.kapsherers.mindvalley_vorathep_android_test;

import android.app.Application;

/**
 * Created by vorathepsumetphong on 1/21/17.
 */
public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}